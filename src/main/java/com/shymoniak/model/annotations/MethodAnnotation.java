package com.shymoniak.model.annotations;

import java.lang.annotation.*;


@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MethodAnnotation {
    int intVal() default 25;
    String strVal() default "Hello World";
}