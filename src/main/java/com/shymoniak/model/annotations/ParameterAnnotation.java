package com.shymoniak.model.annotations;

import java.lang.annotation.*;

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface ParameterAnnotation {
    int intVal() default 25;
    String strVal() default "Hello World";
}
