package com.shymoniak.model.entity;

import lombok.*;

import java.util.Arrays;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RandClass {
    private int intVal;
    private char charVal;
    private double doubleVal;
    private boolean booleanVal;
    private String stringVal1;
    private String stringVal2;

    public void printSmth1(){
        System.out.println("KHEllo!!1!1!!11");
    }

    public void printSmth2(){
        System.out.println("KHEllo!!1!1!!11 Again0)0000))0");
    }

    public void printSmth3(){
        System.out.println("KAvo?");
    }

    public void myMethod(String a, int ... args){
        System.out.println("Method with int args");
        System.out.println(a + "");
        Arrays.stream(args).forEach(arg -> System.out.print(arg + ", "));
        System.out.println("\n");
    }

    public void myMethod(String ... args){
        System.out.println("Method with string args");
        Arrays.stream(args).forEach(a -> System.out.print(a + ", "));
        System.out.println("\n");
    }
}
