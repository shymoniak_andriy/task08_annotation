package com.shymoniak.model.tasks;

import com.shymoniak.model.entity.RandClass;

import java.lang.reflect.Field;

/**
 * Set value into field not knowing its type.
 */
public class Task4 {
    RandClass randObj =
            new RandClass(50, 'c',15.25 ,  false, "value 1", "value 2");

    public void changeValue() {
        Class cls = randObj.getClass();
        Field[] fields = cls.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            Class<?> type = field.getType();

            try {
                System.out.println("old value: " + field.get(randObj));
                if (type.getSimpleName().equals("String")) {
                    field.set(randObj, "NEW VALUE");
                } else if (type.getSimpleName().equals("int")){
                    field.setInt(randObj, 228);
                } else if (type.getSimpleName().equals("char")){
                    field.setChar(randObj, 'q');
                } else if (type.getSimpleName().equals("boolean")){
                    field.setBoolean(randObj, true);
                } else if (type.getSimpleName().equals("double")){
                    field.setDouble(randObj, 45.0);
                }
                System.out.println("new value: " + field.get(randObj));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
