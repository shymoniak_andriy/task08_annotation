package com.shymoniak.model.tasks;

import com.shymoniak.model.annotations.FieldAnnotation;
import com.shymoniak.model.annotations.MethodAnnotation;
import com.shymoniak.model.annotations.ParameterAnnotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Create your own annotation. Create class with a few fields, some of
 * which annotate with this annotation. Through reflection print those
 * fields in the class that were annotate by this annotation.
 */
public class Task1 {


    /**
     * Example of field annotation
     */
    @FieldAnnotation
    private int myInt1;

    @FieldAnnotation
    private String myStr1;

    @FieldAnnotation(intVal = 54)
    private int myInt2;

    @FieldAnnotation(strVal = "new String val")
    private String myStr2;

    public void printValues1() {
        System.out.println("default int  = " + myInt1);
        System.out.println("default String  = " + myStr1);
        System.out.println("new int  = " + myInt2);
        System.out.println("new String  = " + myStr2);
    }

    /**
     * Example of method annotation
     *
     * @throws NoSuchMethodException
     */
    @MethodAnnotation
    public void printValues2()  {
        Method method = null;
        try {
            method = Task1.class.getMethod("printValues2");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Annotation[] annotations = method.getDeclaredAnnotations();

        for (Annotation annotation : annotations) {
            if (annotation instanceof MethodAnnotation) {
                MethodAnnotation myAnnotation = (MethodAnnotation) annotation;
                System.out.println("default int value: " + myAnnotation.intVal());
                System.out.println("default String value: " + myAnnotation.strVal());
            }
        }
    }

    /**
     * Example of parameter annotation
     *
     * @param parameter random String parameter
     * @throws NoSuchMethodException
     */
    public void printValues3(@ParameterAnnotation(intVal = 30, strVal = "new Stringval") String parameter) {
        Method method = null;
        try {
            method = Task1.class.getMethod("printValues3", String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        Class[] parameterTypes = method.getParameterTypes();

        int i = 0;
        for (Annotation[] annotations : parameterAnnotations) {
            Class parameterType = parameterTypes[i++];

            for (Annotation annotation : annotations) {
                if (annotation instanceof ParameterAnnotation) {
                    ParameterAnnotation myAnnotation = (ParameterAnnotation) annotation;
                    System.out.println("param: " + parameterType.getName());
                    System.out.println("default String value: " + myAnnotation.intVal());
                    System.out.println("default String value: " + myAnnotation.strVal());
                }
            }
        }
    }
}