package com.shymoniak.model.tasks;

import com.shymoniak.model.entity.RandClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Invoke myMethod(String a, int ... args) and myMethod(String … args).
 */
public class Task5 {

    public void invokeTwoMethods() {
        RandClass randClass = new RandClass();
        Method[] method = new Method[2];

        try {
            method[0] = RandClass.class.getDeclaredMethod("myMethod", String.class, int[].class);
            method[1] = RandClass.class.getDeclaredMethod("myMethod", String[].class);

            method[0].invoke(randClass, new String("some value"), new int[]{1, 2,3});
            method[1].invoke(randClass, new Object[] {new String[]{"one", "two",  "three"}});
        } catch (NoSuchMethodException ex1 ){
            ex1.printStackTrace();
        } catch (IllegalAccessException ex2){
            ex2.printStackTrace();
        } catch (InvocationTargetException ex3){
            ex3.printStackTrace();
        }
    }

}
