package com.shymoniak.model.tasks;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Create your own class that received object of unknown type and
 * show all information about that Class.
 */
public class Task6 {

    public void classCopy(Object obj){
        Class<?> cls = obj.getClass();
        Method[] methods = cls.getMethods();
        Field[] fields = cls.getDeclaredFields();

        System.out.println("\n\nPrint all object methods: \n");
        Arrays.stream(methods).forEach(m -> {
            m.setAccessible(true);
            System.out.println(m.toString());
        });

        System.out.println("\n\nPrint all object fields: \n");
        Arrays.stream(fields).forEach(f -> {
            f.setAccessible(true);
            System.out.println(f.toString());
        });
        System.out.println(cls);
    }

}
