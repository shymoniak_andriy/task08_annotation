package com.shymoniak.model.tasks;

import com.shymoniak.model.annotations.MethodAnnotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;


/**
 * Print annotation value into console (e.g. @Annotation(name = "111"))
 */
public class Task2 {

    @MethodAnnotation
    public void printAnnotationValues() {
        Method method = null;
        try {
            method = Task2.class.getMethod("printAnnotationValues");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Annotation[] annotations = method.getDeclaredAnnotations();

        for (Annotation annotation: annotations){
            if (annotation instanceof MethodAnnotation) {
                System.out.println(annotation.toString());
            }
        }
    }
}