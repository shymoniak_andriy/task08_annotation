package com.shymoniak.model.tasks;

import com.shymoniak.model.entity.RandClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Invoke method (three method with different parameters and return
 * types)
 */
public class Task3 {
    public void invokeThreeMethods() {
        RandClass randClass = new RandClass();
        Class cls = randClass.getClass();
        Method[] method = new Method[3];

        try {
            method[0] = cls.getDeclaredMethod("printSmth1");
            method[1] = cls.getDeclaredMethod("printSmth2");
            method[2] = cls.getDeclaredMethod("printSmth3");

            method[0].invoke(randClass);
            method[1].invoke(randClass);
            method[2].invoke(randClass);
        } catch (NoSuchMethodException ex1 ){
            ex1.printStackTrace();
        } catch (IllegalAccessException ex2){
            ex2.printStackTrace();
        } catch (InvocationTargetException ex3){
            ex3.printStackTrace();
        }
     }
}
