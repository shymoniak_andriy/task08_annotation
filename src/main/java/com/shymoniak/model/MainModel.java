package com.shymoniak.model;

import com.shymoniak.model.entity.RandClass;
import com.shymoniak.model.tasks.*;

public class MainModel {
    Task1 task1 = new Task1();
    Task2 task2 = new Task2();
    Task3 task3 = new Task3();
    Task4 task4 = new Task4();
    Task5 task5 = new Task5();
    Task6 task6 = new Task6();

    public void printTask1() {
        task1.printValues1();
        task1.printValues2();
        task1.printValues3("any String parameter");
    }

    public void printTask2() {
        task2.printAnnotationValues();
    }

    public void printTask3() {
        task3.invokeThreeMethods();
    }

    public void printTask4() {
        task4.changeValue();
    }

    public void printTask5(){
        task5.invokeTwoMethods();
    }

    public void printTask6(){
        task6.classCopy(new Integer(88));
        task6.classCopy(new RandClass());
    }
}
